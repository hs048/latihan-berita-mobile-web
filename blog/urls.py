from django.urls import path
from blog.views import *
from blog.api import *

urlpatterns = [
    path('', artikel, name="artikel"),
    path('detil/<int:id>', artikel_lihat, name="artikel_lihat"),
    path('filter/<str:nama>', artikel_filter, name="artikel_filter"),


    path('tambah/', artikel_tambah, name="artikel_tambah"),
    path('edit/<int:id>', artikel_edit, name="artikel_edit"),
    path('delete/<int:id>', artikel_delete, name="artikel_delete"),


    path('api/artikel/list/', api_artikel_list),
    path('api/profilku/', profilku),

    path('api/get-news/', get_news),


]
