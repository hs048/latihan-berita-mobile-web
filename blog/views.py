from django.shortcuts import render, redirect
from blog.models import Artikel, Kategori
# Create your views here.
def artikel(request):
    template_name = "artikel/list.html"
    artikel_list = Artikel.objects.all()

    kategori = Kategori.objects.all()
    artikel_terbaru = Artikel.objects.all().order_by('terbit')[:3]
    # print(artikel_list)
    context = {
        'artikel_list': artikel_list,
        'kategori' : kategori,
        'artikel_terbaru' : artikel_terbaru
    }
    return render(request, template_name, context)

def artikel_lihat(request, id):
    template_name =  "artikel/lihat.html"
    artikel = Artikel.objects.get(id=id)

    kategori = Kategori.objects.all()
    artikel_terbaru = Artikel.objects.all().exclude(judul=artikel.judul).order_by('-terbit')[:3]

    # print(artikel)
    context = {
        'artikel' : artikel,
        'kategori' : kategori,
        'artikel_terbaru' : artikel_terbaru
    }
    return render(request, template_name, context)

def artikel_filter(request, nama):
    template_name = "artikel/list.html"
    # kat = Kategori.objects.get(nama=nama)
    artikel_list = Artikel.objects.filter(kategori__nama=nama)

    kategori = Kategori.objects.all()
    artikel_terbaru = Artikel.objects.all().order_by('-terbit')[:3]
    context = {
        'artikel_list': artikel_list,
        'kategori' : kategori,
        'artikel_terbaru' : artikel_terbaru
    }
    return render(request, template_name, context)


def artikel_tambah(request):
    template_name = "artikel/form_tambah.html"
    kategori = Kategori.objects.all()

    if request.method == "POST":
        # print("masuk kedalam post function")
        judul = request.POST.get('judul')
        isi = request.POST.get('isi')
        kategori = request.POST.get('kategori')
        # print(judul, isi, kategori)

        check_kategori = Kategori.objects.get(id=kategori)

        Artikel.objects.create(
            judul = judul,
            isi = isi,
            kategori = check_kategori
        )
        return redirect(artikel)



    context = {
        'kategori' : kategori
    }
    return render(request, template_name, context)

def artikel_edit(request, id):
    template_name = "artikel/form_edit.html"
    artikel = Artikel.objects.get(id=id)
    kategori = Kategori.objects.all()
    if request.method == "POST":
        judul = request.POST.get('judul')
        isi = request.POST.get('isi')
        kategori = request.POST.get('kategori')

        check_kategori = Kategori.objects.get(id=kategori)

        artikel.judul = judul
        artikel.isi = isi
        artikel.kategori = check_kategori
        artikel.save()
        return redirect('artikel')
    
    context = {
        'artikel' : artikel,
        'kategori' : kategori
    }
    return render(request, template_name, context)

def artikel_delete(request, id):
    Artikel.objects.get(id=id).delete()
    return redirect('artikel')
