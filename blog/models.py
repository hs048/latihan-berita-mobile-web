from django.db import models

# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=15)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = "1. Kategori"

class Artikel(models.Model):
    judul = models.CharField(max_length=250, unique=True)
    isi = models.TextField()
    kategori = models.ForeignKey(Kategori, on_delete=models.SET_NULL, blank=True, null=True, related_name='kategori')
    terbit = models.DateTimeField(auto_now_add=True)
    images = models.ImageField(null=True, blank=True)


    is_api = models.BooleanField(default=False)
    url = models.URLField(blank=True, default=True)

    

    def __str__(self):
        return self.judul
    
    class Meta:
        verbose_name_plural = "2. Artikel"