from django.contrib import admin
from blog.models import *
# Register your models here.

admin.site.register(Kategori)

class ArtikelAdmin(admin.ModelAdmin):
    list_display = ('judul', 'kategori', 'terbit')

admin.site.register(Artikel, ArtikelAdmin)
