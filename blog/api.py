from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from blog.models import Artikel, Kategori
from blog.serializers import ArtikelSerializer
import requests
from django.http import HttpResponse

TOKEN = "hs048"

@api_view(['GET','OPSTIONS'])
def api_artikel_list(request):
    try:
        if TOKEN == request.headers['TOKEN']:
       
            artikel = Artikel.objects.all()
            if artikel:
                serializer = ArtikelSerializer(artikel, many=True)
                data = {
                    'jumlah': artikel.count(),
                    'pesan': 'request success',
                    'rows': serializer.data
                }
                return Response(data, status=status.HTTP_200_OK)
            else:
                data = {
                    'jumlah': artikel.count(),
                    'pesan': 'data not found',
                    'rows': serializer.data
                }
                return Response(data, status=status.HTTP_404_NOT_FOUND)
            
        else:
            data = {
                'pesan' : 'TOKEN tidak valid'
            }
            return Response(data, status=status.HTTP_401_UNAUTHORIZED)
    except:
        data = {
            'pesan' : 'masukan TOKEN pada header'
        }
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)
    
@api_view(['GET'])
def profilku(request):
    data = {
        'nama' : "hendra saputra",
        'telp' : "085391540916"
    }
    return Response(data)


##### ------------------------------------------- ####
from django.core.files.base import ContentFile
from io import BytesIO
from django.http import JsonResponse


def download_image(url):
    response = requests.get(url)
    if response.status_code == 200:
        image = response.content
        return ContentFile(image)
    return None

def save_image_to_artikel(url, artikel):
    image_file = download_image(url)
    if image_file:
        artikel.images.save(f'gambar-{artikel.pk}.jpg', image_file)
        artikel.save()
        return True
    return False


def get_news(request):
    url = 'https://newsapi.org/v2/everything?domains=wsj.com&apiKey=7506cc7cfef342f8b740fc5e0112e692'
    params = {
        'country': 'us',
        'category': 'business',
        'apiKey': '7506cc7cfef342f8b740fc5e0112e692'
    }

    response = requests.get(url).json()
    kategori = Kategori.objects.get(nama="sport")
    for r in response['articles']:

        image_file = download_image(r['urlToImage'])

        cek_artikel = Artikel.objects.filter(judul=r['title'])
        if cek_artikel:
            artikel = cek_artikel[0] # karena .filter maka data dalam bentuk array. jadi harus ambil index 0 agar data keluar dari array
            image_url = r['urlToImage'] # url gambarnya...
            
            success = save_image_to_artikel(image_url, artikel) # panggil functins
            if success:
                print('Gambar berhasil diunduh dan disimpan dalam artikel.')
            else:
                print('Gagal mengunduh gambar atau tidak valid.')
        else:
            if r['title']:
                if r['description']:
                    Artikel.objects.create(
                        judul=r['title'],
                        isi = r['description'],
                        kategori = kategori,
                        terbit = r['publishedAt'],
                        is_api = True,
                        url = r['urlToImage'],
                        image =  image_file
                    )
    data = response

    return JsonResponse(data)