from rest_framework import serializers
from blog.models import Artikel

class ArtikelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artikel
        fields = ['id', 'judul', 'isi', 'kategori', 'terbit']