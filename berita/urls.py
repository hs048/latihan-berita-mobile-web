from django.contrib import admin
from django.urls import path, include
from berita.views import index, about

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('artikel/', include('blog.urls')),
   
    path('', index, name="home"),
    path('about', about, name="about"),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
