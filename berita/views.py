from django.shortcuts import render

def index(request):
    template_name = "index.html"
    context = {
        'title' : 'welcome home page'
	}
    return render(request, template_name, context)


def about(request):
    template_name = "about.html"
    context = {
        'title' : 'welcome about page'
	}
    return render(request, template_name, context)